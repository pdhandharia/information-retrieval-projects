import string
from sets import Set
# Iterations for the running the page rank algo
ITR = 10

f = open("Input.txt", "r")

# Set of Pages
setOfPages = Set()

# S is the set of sink nodes, i.e., pages that have no out links
setOfSinkNodes = Set()

# M(p) is the set of pages that link to page p
# {(A,[D,E,F])}
linkToPage = {}

# // L(q) is the number of out-links from page q
outlink = {}

# // d is the PageRank damping/teleportation factor; use d = 0.85 as is typical
d = 0.85

processInput = []

# PageRank associative Array
PR = {}
newPR = {}

for line in f:
    line = string.rstrip(line, "\n")
    line = string.strip(line, " ")
    processInput = string.split(line, " ", -1)
    lengthOfInput = processInput.__len__()
    if lengthOfInput == 0:
        continue
    
    currentNode = processInput[0]
    # adding each page in the set of nodes     
    setOfPages.add(currentNode)  
    
    if not outlink.has_key(currentNode):
        outlink[currentNode] = 0
    
    listOfInLinks = []
    count = 1
    while count < lengthOfInput:
        setOfPages.add(processInput[count])
        listOfInLinks.append(processInput[count])
        if outlink.has_key(processInput[count]):
            outlink[processInput[count]] = outlink[processInput[count]] + 1
        else:
            outlink[processInput[count]] = 1 
        count +=1
    
    if not linkToPage.has_key(currentNode):
        linkToPage[currentNode] = listOfInLinks
        
for element in setOfPages:
    if not linkToPage.has_key(element):
        linkToPage[element] = []
    
for element in outlink:
    if outlink[element] == 0:
        setOfSinkNodes.add(element)

#List of pages
N = len(setOfPages)

for page in setOfPages:
    PR[page] = float(1)/float(N);

iteration = 1

while iteration <= ITR:
    sinkPR = 0
    for p in setOfSinkNodes:
        sinkPR += PR[p]
    for p in setOfPages:
        newPR[p] = float(1-d)/float(N)
        newPR[p] += float(d)*float(sinkPR)/float(N)
        for q in linkToPage[p]:
            newPR[p] += float(d)*float(PR[q])/float(outlink[q])
    for page in setOfPages:
        PR[page] = newPR[page]
    iteration +=1   

print "Page Ranks after " + str(ITR) + " iteration(s) :"
print PR

f.close()