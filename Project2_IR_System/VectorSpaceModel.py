import urllib, re
import string
from sets import Set

f= open("desc.51-100.short","r")
fstopList = open("stoplist.txt","r")

documents = {}

#contains stop list
stopList = Set()

#creating the list of stop words
for line in fstopList:
    line = string.strip(line, "\n")
    stopList.add(line)

for line in f:
    processedWords = []
    words = line.split()
    query_number = words.pop(0)
    query_number = string.strip(query_number, ".")
    
    for one_word in words:
        one_word = one_word.replace("'", "")
        one_word = one_word.replace("-", "")
        one_word = one_word.replace("\"", "")
        one_word = one_word.replace("(", "")
        one_word = one_word.replace(")", "")
        one_word = one_word.replace(".", "")
        one_word = one_word.replace(",", "")
        processedWords.append(string.lower(one_word))
#         .,()\"
    
#     print "Query Number: " + query_number
    print query_number
    print processedWords
    #print words
f.close()
fstopList.close()