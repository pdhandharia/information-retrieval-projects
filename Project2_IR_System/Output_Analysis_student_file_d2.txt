C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrel.irclass10X1 Output\Output_Part1.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:       560
    Rel_ret:        420
Interpolated Recall - Precision Averages:
    at 0.00       0.5693
    at 0.10       0.3991
    at 0.20       0.2949
    at 0.30       0.2127
    at 0.40       0.1822
    at 0.50       0.1629
    at 0.60       0.1353
    at 0.70       0.1212
    at 0.80       0.0998
    at 0.90       0.0784
    at 1.00       0.0559
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.1944
Precision:
  At    5 docs:   0.2480
  At   10 docs:   0.1960
  At   15 docs:   0.1787
  At   20 docs:   0.1740
  At   30 docs:   0.1520
  At  100 docs:   0.0844
  At  200 docs:   0.0552
  At  500 docs:   0.0292
  At 1000 docs:   0.0168
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2051

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrel.irclass10X1 Output\Output_Part2.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:       560
    Rel_ret:        449
Interpolated Recall - Precision Averages:
    at 0.00       0.5459
    at 0.10       0.4465
    at 0.20       0.3318
    at 0.30       0.2440
    at 0.40       0.2233
    at 0.50       0.2020
    at 0.60       0.1810
    at 0.70       0.1649
    at 0.80       0.1420
    at 0.90       0.1050
    at 1.00       0.0763
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.2251
Precision:
  At    5 docs:   0.2800
  At   10 docs:   0.2280
  At   15 docs:   0.1973
  At   20 docs:   0.1900
  At   30 docs:   0.1747
  At  100 docs:   0.1072
  At  200 docs:   0.0694
  At  500 docs:   0.0332
  At 1000 docs:   0.0180
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2301

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrel.irclass10X1 Output\Output_Part3.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:       560
    Rel_ret:        401
Interpolated Recall - Precision Averages:
    at 0.00       0.4504
    at 0.10       0.3131
    at 0.20       0.2097
    at 0.30       0.1743
    at 0.40       0.1372
    at 0.50       0.1211
    at 0.60       0.0943
    at 0.70       0.0801
    at 0.80       0.0593
    at 0.90       0.0388
    at 1.00       0.0220
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.1409
Precision:
  At    5 docs:   0.2560
  At   10 docs:   0.2080
  At   15 docs:   0.1973
  At   20 docs:   0.1780
  At   30 docs:   0.1573
  At  100 docs:   0.0892
  At  200 docs:   0.0590
  At  500 docs:   0.0282
  At 1000 docs:   0.0160
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.1598

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrel.irclass10X1 Output\Output_Part4.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:       560
    Rel_ret:        519
Interpolated Recall - Precision Averages:
    at 0.00       0.6151
    at 0.10       0.4091
    at 0.20       0.3719
    at 0.30       0.2979
    at 0.40       0.2709
    at 0.50       0.2458
    at 0.60       0.2168
    at 0.70       0.1998
    at 0.80       0.1715
    at 0.90       0.1358
    at 1.00       0.1002
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.2495
Precision:
  At    5 docs:   0.2720
  At   10 docs:   0.2640
  At   15 docs:   0.2133
  At   20 docs:   0.2100
  At   30 docs:   0.1800
  At  100 docs:   0.1172
  At  200 docs:   0.0766
  At  500 docs:   0.0384
  At 1000 docs:   0.0208
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2561

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrel.irclass10X1 Output\Output_Part5.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:       560
    Rel_ret:        522
Interpolated Recall - Precision Averages:
    at 0.00       0.6751
    at 0.10       0.5359
    at 0.20       0.4770
    at 0.30       0.3566
    at 0.40       0.3206
    at 0.50       0.2982
    at 0.60       0.2555
    at 0.70       0.2268
    at 0.80       0.1821
    at 0.90       0.1461
    at 1.00       0.1189
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.3053
Precision:
  At    5 docs:   0.4000
  At   10 docs:   0.3120
  At   15 docs:   0.2800
  At   20 docs:   0.2560
  At   30 docs:   0.2307
  At  100 docs:   0.1332
  At  200 docs:   0.0848
  At  500 docs:   0.0392
  At 1000 docs:   0.0209
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.3007

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>