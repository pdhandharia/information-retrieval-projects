from sets import Set
import string
fstopList = open("stoplist.txt","r")

fstemClasses = open("stem-classes.lst","r")

fdoclist = open("doclist.txt","r")
internalToExternalDocIdMap = {}

stopList = Set()
stemClass = {}

def addToStemClass(key,value):
#     if not stemClass.has_key(key):
        stemClass[key] = value

for line in fstopList:
    line = string.strip(line, "\n")
    stopList.add(line)

# test  = "about"
# if stopList.__contains__(test):
#     print "found"
    
for stem in fstemClasses:
    stem = string.rstrip(stem, "\n")
    stems = string.split(stem, "|", -1)
    stems = map(lambda x : string.strip(x," "), stems)
    key = stems.pop(0)
    map(lambda value: addToStemClass(value,key), string.split(stems.pop(0)," ",-1))


for doc in fdoclist:
    docs = doc.split()
    if docs[0] == "44004":
        print docs[0]
    internalToExternalDocIdMap[docs[0]] = docs[1]

print len(internalToExternalDocIdMap)
fdoclist.close()