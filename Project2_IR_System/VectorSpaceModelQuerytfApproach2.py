import string
from sets import Set
import urllib, re
from collections import OrderedDict

f = open("desc.51-100.short","r")
fstopList = open("stoplist.txt","r")

fdoclist = open("doclist.txt","r")
internalToExternalDocIdMap = {}

#contains stop list
stopList = Set()

listOfWords = []

QueryMappedWithNumbers = {}
QueryLength = {}

termWeightPerTerm = {}

avg_doclen = 288

wordsNotToModify = ["U.S."]

for doc in fdoclist:
    docs = doc.split()
    internalToExternalDocIdMap[int(docs[0])] = docs[1]

#creating the list of stop words
for line in fstopList:
    line = string.strip(line, "\n")
    stopList.add(line)

for line in f:
    
    words = line.split()
    query_number = words.pop(0)
    query_number = int(string.strip(query_number, "."))
        
    count = 0
    TermsPerQuery = []
    for single_word in words:
        if not stopList.__contains__(single_word):
            if not wordsNotToModify.__contains__(single_word):
                single_word = string.strip(single_word, ".,()\"")
            TermsPerQuery.append(single_word)
            count+=1
    QueryMappedWithNumbers[query_number] = TermsPerQuery
    QueryLength[query_number] = count

OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

avgQueryLength = sum(QueryLength.values())/len(QueryLength)

foutput = open("Output/Output_Part1_Approach2.txt", "w")

for (key,value) in OrderedQueriesMappedWithNumbers.items():
    
    DocumentstermWeightPerTerm = {}
    query_number = key
    current_query_length = len(value)
    print query_number
    for term in value:
        okapiTF_query_curr_term = value.count(term) / (value.count(term) + 0.5 + 1.5*current_query_length/avgQueryLength)
        
        text = urllib.urlopen("http://fiji4.ccs.neu.edu/~zerg/lemurcgi/lemur.cgi?d=3&g=p&v="+ str(term)).read()
        data = re.compile(r'.*?<BODY>(.*?)<HR>', re.DOTALL).match(text).group(1)
        numbers = re.compile(r'(\d+)',re.DOTALL).findall(data)

        inverted_list = map(lambda i: (int(numbers[2 + 3*i]),
                                  float(numbers[3 + 3*i]),
                               float(numbers[4 + 3*i]))
                        ,range(0, (len(numbers) - 2)/3))
    
        for (docid,doclen,tf) in inverted_list:
            okapiTF_document_curr_term = tf / (tf + 0.5 + 1.5*doclen/avg_doclen)
            if DocumentstermWeightPerTerm.has_key(docid):
                DocumentstermWeightPerTerm[docid] += okapiTF_document_curr_term*okapiTF_query_curr_term                
            else: 
                DocumentstermWeightPerTerm[docid] = okapiTF_document_curr_term*okapiTF_query_curr_term
    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
    
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + internalToExternalDocIdMap[key] + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1

foutput.close()
fdoclist.close()    
fstopList.close()
f.close()            