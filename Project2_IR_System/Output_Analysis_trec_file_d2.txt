C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrels.adhoc.51-100.AP89 Output\Output_Part1.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:      1832
    Rel_ret:        935
Interpolated Recall - Precision Averages:
    at 0.00       0.5439
    at 0.10       0.3644
    at 0.20       0.2532
    at 0.30       0.2108
    at 0.40       0.1692
    at 0.50       0.1315
    at 0.60       0.1021
    at 0.70       0.0855
    at 0.80       0.0547
    at 0.90       0.0357
    at 1.00       0.0035
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.1572
Precision:
  At    5 docs:   0.2720
  At   10 docs:   0.2440
  At   15 docs:   0.2533
  At   20 docs:   0.2500
  At   30 docs:   0.2227
  At  100 docs:   0.1504
  At  200 docs:   0.1098
  At  500 docs:   0.0629
  At 1000 docs:   0.0374
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.1907

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrels.adhoc.51-100.AP89 Output\Output_Part2.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:      1832
    Rel_ret:       1008
Interpolated Recall - Precision Averages:
    at 0.00       0.5544
    at 0.10       0.4168
    at 0.20       0.2849
    at 0.30       0.2541
    at 0.40       0.2207
    at 0.50       0.1820
    at 0.60       0.1548
    at 0.70       0.1283
    at 0.80       0.0817
    at 0.90       0.0545
    at 1.00       0.0126
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.1944
Precision:
  At    5 docs:   0.2640
  At   10 docs:   0.2800
  At   15 docs:   0.2640
  At   20 docs:   0.2620
  At   30 docs:   0.2507
  At  100 docs:   0.1728
  At  200 docs:   0.1264
  At  500 docs:   0.0704
  At 1000 docs:   0.0403
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2160

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrels.adhoc.51-100.AP89 Output\Output_Part3.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:      1832
    Rel_ret:        833
Interpolated Recall - Precision Averages:
    at 0.00       0.5067
    at 0.10       0.2784
    at 0.20       0.1920
    at 0.30       0.1494
    at 0.40       0.1112
    at 0.50       0.0822
    at 0.60       0.0686
    at 0.70       0.0456
    at 0.80       0.0280
    at 0.90       0.0124
    at 1.00       0.0016
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.1158
Precision:
  At    5 docs:   0.2640
  At   10 docs:   0.2160
  At   15 docs:   0.2213
  At   20 docs:   0.2060
  At   30 docs:   0.1907
  At  100 docs:   0.1300
  At  200 docs:   0.0946
  At  500 docs:   0.0538
  At 1000 docs:   0.0333
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.1436

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrels.adhoc.51-100.AP89 Output\Output_Part4.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:      1832
    Rel_ret:       1181
Interpolated Recall - Precision Averages:
    at 0.00       0.5405
    at 0.10       0.3923
    at 0.20       0.3379
    at 0.30       0.2999
    at 0.40       0.2542
    at 0.50       0.2204
    at 0.60       0.1795
    at 0.70       0.1572
    at 0.80       0.1203
    at 0.90       0.0806
    at 1.00       0.0256
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.2208
Precision:
  At    5 docs:   0.3280
  At   10 docs:   0.3160
  At   15 docs:   0.2933
  At   20 docs:   0.2820
  At   30 docs:   0.2653
  At  100 docs:   0.1968
  At  200 docs:   0.1396
  At  500 docs:   0.0783
  At 1000 docs:   0.0472
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2440

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>trec_eval.pl qrels.adhoc.51-100.AP89 Output\Output_Part5.txt

Queryid (Num):       25
Total number of documents over all queries
    Retrieved:    25000
    Relevant:      1832
    Rel_ret:       1218
Interpolated Recall - Precision Averages:
    at 0.00       0.6452
    at 0.10       0.4228
    at 0.20       0.3844
    at 0.30       0.3278
    at 0.40       0.2778
    at 0.50       0.2315
    at 0.60       0.1997
    at 0.70       0.1774
    at 0.80       0.1351
    at 0.90       0.0910
    at 1.00       0.0315
Average precision (non-interpolated) for all rel docs(averaged over queries)
                  0.2461
Precision:
  At    5 docs:   0.3680
  At   10 docs:   0.3480
  At   15 docs:   0.3333
  At   20 docs:   0.3240
  At   30 docs:   0.3027
  At  100 docs:   0.2080
  At  200 docs:   0.1452
  At  500 docs:   0.0815
  At 1000 docs:   0.0487
R-Precision (precision after R (= num_rel for a query) docs retrieved):
    Exact:        0.2725

C:\Users\Pratik\Documents\IR_Projects\ir-pagerank\Project2_IR_System>