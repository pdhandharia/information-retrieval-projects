import string
from sets import Set
import urllib, re
from collections import OrderedDict
import math

f = open("desc.51-100.short","r")
fstopList = open("stoplistOriginal.txt","r")

fdoclist = open("doclist.txt","r")
internalToExternalDocIdMap = {}

excludeWords = []

#contains stop list
stopList = Set()

QueryMappedWithNumbers = {}

db = 3

database = [[84678,41802513,207615,493],
            [84678,41802513,166242,493],
            [84678,24401877,207224,288],
            [84678,24401877,166054,288]]

avg_doclen = float(database[db][3])
totalNumDocs = float(database[db][0])
numOfUniqueWords = float(database[db][2])

for doc in fdoclist:
    docs = doc.split()
    internalToExternalDocIdMap[int(docs[0])] = docs[1]

#creating the list of stop words
for line in fstopList:
    line = string.strip(line, "\n")
    stopList.add(line)

for line in f:
    processedWords = []
    words = line.split()
    query_number = words.pop(0)
    query_number = int(string.strip(query_number, "."))
     
    for one_word in words:
        one_word = one_word.replace("\"", "")
        one_word = one_word.replace("(", "")
        one_word = one_word.replace(")", "")
        one_word = one_word.replace(".", "")
        one_word = one_word.replace(",", "")
         
        if string.count(one_word,"'") > 0:
            splitWordsbyColon = string.split(one_word, "'",-1)
            map(lambda x : processedWords.append(string.lower(x)), splitWordsbyColon)
        elif string.count(one_word,"-") > 0:
            splitWordsbyHyphen = string.split(one_word, "-",-1)
            map(lambda x : processedWords.append(string.lower(x)), splitWordsbyHyphen)
        else: 
            processedWords.append(string.lower(one_word))
         
 
    TermsPerQuery = []
    #print words
    for single_word in processedWords:
        if not stopList.__contains__(single_word):
            TermsPerQuery.append(single_word)
    QueryMappedWithNumbers[query_number] = TermsPerQuery
    
OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

foutput = open("Output/Output_Part5.txt", "w")   
 
for (key,value) in OrderedQueriesMappedWithNumbers.items():
     
    DocumentstermWeightPerTerm = {}
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:
                
        text = urllib.urlopen("http://fiji5.ccs.neu.edu/~zerg/lemurcgi/lemur.cgi?d=" + str(db) + "&g=p&v="+ term).read()
        data = re.compile(r'.*?<BODY>(.*?)<HR>', re.DOTALL).match(text).group(1)
        numbers = re.compile(r'(\d+)',re.DOTALL).findall(data)
 
        inverted_list = map(lambda i: (int(numbers[2 + 3*i]),
                                  float(numbers[3 + 3*i]),
                               float(numbers[4 + 3*i]))
                        ,range(0, (len(numbers) - 2)/3))
        
        ctf,df = float(numbers[0]), float(numbers[1])
        
        for (docid,doclen,tf) in inverted_list:
            k = 1.2*(0.25 + 0.75*doclen/avg_doclen)
            bm_n_1 = math.log((totalNumDocs - df + 0.5)/(df + 0.5))
            bm_n_2 = ((2.2*tf)*(101*value.count(term)))/((tf+k)*(100 + value.count(term)))
            
#             P_lm_term_n = (totalNumDocs - df + 0.5)*(2.2*tf)*(101*value.count(term))
#             k = 1.2*(0.25 + 0.75*doclen/avg_doclen)
#             P_lm_term_d = (df + 0.5)*(tf+k)*(100 + value.count(term))
#             
#             p_lm_term = P_lm_term_n/P_lm_term_d
            
            #/(current_query_length*doclen)
            if DocumentstermWeightPerTerm.has_key(docid):
                DocumentstermWeightPerTerm[docid] += bm_n_1 * bm_n_2                
            else: 
                DocumentstermWeightPerTerm[docid] = bm_n_1 * bm_n_2

    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
     
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + internalToExternalDocIdMap[key] + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1
 
foutput.close()
fdoclist.close()    
fstopList.close()
f.close()            