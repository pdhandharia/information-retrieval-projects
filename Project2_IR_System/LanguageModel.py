import string
from sets import Set
import urllib, re
from collections import OrderedDict
import math

f = open("desc.51-100.short","r")
fstopList = open("stoplistOriginal.txt","r")

fdoclist = open("doclist.txt","r")
internalToExternalDocIdMap = {}

excludeWords = []

#contains stop list
stopList = Set()

QueryMappedWithNumbers = {}

db = 3

database = [[84678,41802513,207615,493],
            [84678,41802513,166242,493],
            [84678,24401877,207224,288],
            [84678,24401877,166054,288]]

avg_doclen = float(database[db][3])
totalNumDocs = float(database[db][0])
numOfUniqueWords = float(database[db][2])

for doc in fdoclist:
    docs = doc.split()
    internalToExternalDocIdMap[int(docs[0])] = docs[1]

#creating the list of stop words
for line in fstopList:
    line = string.strip(line, "\n")
    stopList.add(line)

for line in f:
    processedWords = []
    words = line.split()
    query_number = words.pop(0)
    query_number = int(string.strip(query_number, "."))
    
    for one_word in words:
        one_word = one_word.replace("\"", "")
        one_word = one_word.replace("(", "")
        one_word = one_word.replace(")", "")
        one_word = one_word.replace(".", "")
        one_word = one_word.replace(",", "")
         
        if string.count(one_word,"'") > 0:
            splitWordsbyColon = string.split(one_word, "'",-1)
            map(lambda x : processedWords.append(string.lower(x)), splitWordsbyColon)
        elif string.count(one_word,"-") > 0:
            splitWordsbyHyphen = string.split(one_word, "-",-1)
            map(lambda x : processedWords.append(string.lower(x)), splitWordsbyHyphen)
        else: 
            processedWords.append(string.lower(one_word))

    TermsPerQuery = []
    #print words
    for single_word in processedWords:
        if not stopList.__contains__(single_word):
            TermsPerQuery.append(single_word)
    QueryMappedWithNumbers[query_number] = TermsPerQuery
    
OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

foutput = open("Output/Output_Part3.txt", "w")   

for (key,value) in OrderedQueriesMappedWithNumbers.items():
    DocumentstermWeightPerTerm = OrderedDict()
    TermCountInDocument = OrderedDict()
    DocLengthHash = OrderedDict()
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:        
        text = urllib.urlopen("http://fiji5.ccs.neu.edu/~zerg/lemurcgi/lemur.cgi?d="+ str(db) +"&g=p&v="+ term).read()
        data = re.compile(r'.*?<BODY>(.*?)<HR>', re.DOTALL).match(text).group(1)
        numbers = re.compile(r'(\d+)',re.DOTALL).findall(data)

        inverted_list = map(lambda i: (int(numbers[2 + 3*i]),
                                  float(numbers[3 + 3*i]),
                               float(numbers[4 + 3*i]))
                        ,range(0, (len(numbers) - 2)/3))
        for (docid,doclen,tf) in inverted_list:
            DocLengthHash[docid] = doclen
            P_lm_term = math.log((tf + 1)/(doclen + 166054))
            if DocumentstermWeightPerTerm.has_key(docid):
                DocumentstermWeightPerTerm[docid] += P_lm_term
                TermCountInDocument[docid] = TermCountInDocument[docid] + 1                
            else:
                DocumentstermWeightPerTerm[docid] = P_lm_term
                TermCountInDocument[docid] = 1
    
    for doc in TermCountInDocument.keys():
        num = current_query_length - TermCountInDocument[doc]
        doclength_unseen_terms = DocLengthHash[doc]
        if num > 0:
            for x in range(1,num):
                DocumentstermWeightPerTerm[doc] += math.log(1/(doclength_unseen_terms + 166054))

    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
    
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
    	foutput.write(str(query_number) + " Q0 " + internalToExternalDocIdMap[key] + " " + str(rank) + " " + str(value) + " Exp\n")
    	if rank == 1000:
    		break
    	rank +=1

foutput.close()
fdoclist.close()    
fstopList.close()
f.close()            