import operator
from decimal import *
f = open("output","r")

i = 0
top25SortedWordFreqList = []
ftop25SortedWordFreqList = []
wordFreq = {}
probabilityList = {}
wordRank = {}

for line in f:
    line = line.replace('\n','')
    if wordFreq.has_key(line):
        wordFreq[line] = wordFreq[line] + 1
    else:
        wordFreq[line] = 1
    i = i+1
    
print "-" * 50
print "Total count" , i

f.close()

sortedWordFreqList = sorted(wordFreq.items(), key=operator.itemgetter(1),reverse=True)

print "-" * 50
print "Total Unique word count - " , len(sortedWordFreqList)

# print sortedWordFreqList

count = 1
for word in sortedWordFreqList:
    if count == 26:
        break 
    top25SortedWordFreqList.append(word[0])
    count = count + 1

# print top25SortedWordFreqList

count = 1
for word in sortedWordFreqList:
    if count == 26:
        break 
    if word[0].startswith('f') == True and word[0] not in top25SortedWordFreqList and word[0] not in ftop25SortedWordFreqList:
        ftop25SortedWordFreqList.append(word[0])
        count = count + 1

# print ftop25SortedWordFreqList

getcontext().prec = 4

for word in top25SortedWordFreqList:
    probabilityList[word] = Decimal(wordFreq[word])/Decimal(i)
    wordRank[word] = sortedWordFreqList.index((word,wordFreq[word])) +1

for word in ftop25SortedWordFreqList:
    probabilityList[word] = Decimal(wordFreq[word])/Decimal(i)
    wordRank[word] = sortedWordFreqList.index((word,wordFreq[word]))+1
    
# print probabilityList
# print wordRank

print "-" * 50
print "Top 25 Most Frequent words"
print "Word \t\t Frequency \t Rank \t Probability \t Product"
for word in top25SortedWordFreqList:
    print word , "\t\t" , wordFreq[word] , " \t\t" , wordRank[word], " \t ", probabilityList[word], " \t ", wordRank[word]*probabilityList[word] 

print "-" * 50
print "Top 25 Next Frequent words starting with f"
print "Word \t\t Frequency \t Rank \t Probability \t Product"
for word in ftop25SortedWordFreqList:
    print word , '\t\t' , wordFreq[word] , '\t\t' , wordRank[word], '\t', probabilityList[word], '\t', wordRank[word]*probabilityList[word] 

count = 0
for word in wordFreq.keys():
    if wordFreq[word] < 5:
        count = count + 1 

print "-" * 50
print "Count of words with frequency less than 5"
print count

print "-" * 50
print "Proportion of words with frequency less than 5"
print Decimal(count)/Decimal(2632)

# top 25 frequent word plus top 25 frequent starting with f
# word - frequency - Rank - probability - rank * probability