import os
import time

if not os.path.exists("IndexFullNoStopping"):
    os.mkdir("IndexFullNoStopping")
time.sleep(1)

firstIndex = open("IndexFullNoStopping/firstIndex","w")
secondIndex = open("IndexFullNoStopping/secondIndex","w")

listOfindexes = os.listdir("indexNoStopping")

print len(listOfindexes)

offset = 1

for index in listOfindexes:
    with open("indexNoStopping/" + index,"r") as f:
        numOfLines = 0
        ctf = 0
        for line in f:
            numOfLines += 1
            secondIndex.write(line)
            ctf += int(line.split()[2])
    print index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf)
    firstIndex.write(index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf) + "\n")
    offset += numOfLines

firstIndex.close()
secondIndex.close()
