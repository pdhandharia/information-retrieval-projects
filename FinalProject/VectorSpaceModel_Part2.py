from sets import Set
from collections import OrderedDict
import math
from ReadingIndexNoStopping import ReadIndex
from ParseQuery import ParseQuery
import os

r = ReadIndex()
pq = ParseQuery()
rootDir = "cacm"

QueryMappedWithNumbers = {}
QueryLength = {}
QueryWords = Set()
QueryIDF = {}

avg_doclen = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[0])
totalNumDocs = len(os.listdir(rootDir))

QueryMappedWithNumbers = pq.getParsedQuery()

for value in QueryMappedWithNumbers.values():
    map(lambda x: QueryWords.add(x), value)        

for key,value in QueryMappedWithNumbers.items():
    QueryLength[key] = len(value)

OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

avgQueryLength = float(sum(QueryLength.values()))/len(QueryLength)

print avgQueryLength

QueryN = len(OrderedQueriesMappedWithNumbers)

for queryWord in QueryWords:
    for value in QueryMappedWithNumbers.values():
        if value.__contains__(queryWord):
            if QueryIDF.has_key(queryWord):
                QueryIDF[queryWord] += 1
            else:
                QueryIDF[queryWord] = 1

if not os.path.exists("OutputQueryNS_DB_S"):
    os.mkdir("OutputQueryNS_DB_S")

foutput = open("OutputQueryNS_DB_S/Output_Part2.txt", "w")   
 
for (key,value) in OrderedQueriesMappedWithNumbers.items():
     
    DocumentstermWeightPerTerm = {}
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:
        okapiTF_query_curr_term = float(value.count(term)) / (float(value.count(term)) + 0.5 + 1.5*current_query_length/avgQueryLength)
#         okapiIDF_query_curr_term = math.log(QueryN/QueryIDF[term])
        numbers = r.read(term)
        if len(numbers) > 0:
            inverted_list = map(lambda i: (numbers[2 + 3*i],
                                      float(numbers[3 + 3*i]),
                                   float(numbers[4 + 3*i]))
                            ,range(0, (len(numbers) - 2)/3))
            
            df,ctf = float(numbers[0]), float(numbers[1]) #take the ctf and df and convert to float
            if df > 0:
                okapiIDF_document_curr_term = math.log(totalNumDocs/df)
                
                for (docid,doclen,tf) in inverted_list:
                    okapiTF_document_curr_term = tf / (tf + 0.5 + 1.5*doclen/avg_doclen)
                    
                    tempValue = (okapiTF_query_curr_term*okapiTF_document_curr_term*okapiIDF_document_curr_term)
                    #/(current_query_length*doclen)
                    
                    if DocumentstermWeightPerTerm.has_key(docid):
                        DocumentstermWeightPerTerm[docid] += tempValue                
                    else: 
                        DocumentstermWeightPerTerm[docid] = tempValue
    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
     
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + key + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1
 
foutput.close()