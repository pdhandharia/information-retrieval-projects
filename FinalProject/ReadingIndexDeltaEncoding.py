import linecache

class ReadIndex:
      
    def read(self,word):
        docidMap = {}
        docidDoclenMap = {}
        d = open("IndexFullDeltaEncoding/documentMap","r")
        
        for line in d:
            words = line.split()
            docidMap[int(words[0])] = words[1]
            docidDoclenMap[int(words[0])] = words[2]
        
        f = open("IndexFullDeltaEncoding/firstIndex","r")
        
        output = []
        for line in f:
            words = line.split()
            if words[0] == word:
#                 print words[0] + " - " + words[2] + " - " + words[3]
                output.append(float(words[2]))
                output.append(float(words[3]))
                n = int(words[2])
                offset = int(words[1])
                tmpdoc = 0
                while n >0:
                    temp = linecache.getline("IndexFullDeltaEncoding/secondIndex", offset).strip().split()
                    tmpdoc += int(temp[0])
                    output.extend([docidMap[tmpdoc],docidDoclenMap[tmpdoc],temp[1]])
                    offset += 1      
                    n -= 1
        return output