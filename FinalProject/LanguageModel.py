from collections import OrderedDict
import math
from ReadingIndexNoStopping import ReadIndex
from ParseQuery import ParseQuery
import os

r = ReadIndex()
pq = ParseQuery()
rootDir = "cacm"
QueryMappedWithNumbers = {}

avg_doclen = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[0])
totalNumDocs = len(os.listdir(rootDir))
numOfUniqueWords = len(os.listdir("indexNoStopping"))

QueryMappedWithNumbers = pq.getParsedQuery()
    
OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

if not os.path.exists("OutputQueryNS_DB_S"):
    os.mkdir("OutputQueryNS_DB_S")
    
foutput = open("OutputQueryNS_DB_S/Output_Part3.txt", "w")   

for (key,value) in OrderedQueriesMappedWithNumbers.items():
    DocumentstermWeightPerTerm = OrderedDict()
    TermCountInDocument = OrderedDict()
    DocLengthHash = OrderedDict()
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:        
        numbers = r.read(term)
        
        if len(numbers) > 0:
            inverted_list = map(lambda i: (numbers[2 + 3*i],
                                      float(numbers[3 + 3*i]),
                                   float(numbers[4 + 3*i]))
                            ,range(0, (len(numbers) - 2)/3))
            
            for (docid,doclen,tf) in inverted_list:
                DocLengthHash[docid] = doclen
                P_lm_term = math.log((tf + 1)/(doclen + 166054))
                if DocumentstermWeightPerTerm.has_key(docid):
                    DocumentstermWeightPerTerm[docid] += P_lm_term
                    TermCountInDocument[docid] = TermCountInDocument[docid] + 1                
                else:
                    DocumentstermWeightPerTerm[docid] = P_lm_term
                    TermCountInDocument[docid] = 1
    
    for doc in TermCountInDocument.keys():
        num = current_query_length - TermCountInDocument[doc]
        doclength_unseen_terms = DocLengthHash[doc]
        if num > 0:
            for x in range(1,num):
                DocumentstermWeightPerTerm[doc] += math.log(1/(doclength_unseen_terms + 166054))

    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + key + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1

foutput.close()