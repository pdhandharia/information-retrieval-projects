from collections import OrderedDict
import math
from ReadingIndexNoStopping import ReadIndex
from ParseQuery import ParseQuery
import os

r = ReadIndex()
pq = ParseQuery()
rootDir = "cacm"

QueryMappedWithNumbers = {}

avg_doclen = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[0])
totalNumDocs = len(os.listdir(rootDir))
numOfUniqueWords = len(os.listdir("indexNoStopping"))

#creating the list of stop words
QueryMappedWithNumbers = pq.getParsedQuery()

OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

if not os.path.exists("OutputQueryNS_DB_S"):
    os.mkdir("OutputQueryNS_DB_S")

foutput = open("OutputQueryNS_DB_S/Output_Part5.txt", "w")   
 
for (key,value) in OrderedQueriesMappedWithNumbers.items():
     
    DocumentstermWeightPerTerm = {}
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:
                
        numbers = r.read(term)
        if len(numbers) > 0:     
            inverted_list = map(lambda i: (numbers[2 + 3*i],
                                      float(numbers[3 + 3*i]),
                                   float(numbers[4 + 3*i]))
                            ,range(0, (len(numbers) - 2)/3))
            
            df,ctf = float(numbers[0]), float(numbers[1])
            
            for (docid,doclen,tf) in inverted_list:
                k = 1.2*(0.25 + 0.75*doclen/avg_doclen)
                bm_n_1 = math.log(float(totalNumDocs - df + 0.5)/float(df + 0.5))
                bm_n_2 = ((2.2*tf)*(101*value.count(term)))/((tf+k)*(100 + value.count(term)))
                
                #/(current_query_length*doclen)
                if DocumentstermWeightPerTerm.has_key(docid):
                    DocumentstermWeightPerTerm[docid] += bm_n_1 * bm_n_2                
                else: 
                    DocumentstermWeightPerTerm[docid] = bm_n_1 * bm_n_2

    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
     
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + key + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1
 
foutput.close()