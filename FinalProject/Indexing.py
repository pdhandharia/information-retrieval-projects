import re,os
from sets import Set
import string,shutil,time
from PorterStem import PorterStemmer

p = PorterStemmer()
rootDir = "cacm"
invalidDir = ["con"]
doclen = {}

shutil.rmtree("index")
time.sleep(3)
os.mkdir("index")

regex = "CA\d{6}\s{1}\w{2}\s{1}"
#regex = ".\number{1}\s{1}\number{1}\t{1}\number{1}"
pattern = re.compile(r'\W+')
f_stoplist = open("stoplist.txt","r")
stoplist = Set()
for line in f_stoplist:
    line = string.strip(line, "\n")
    stoplist.add(line)

listOfDocs = os.listdir(rootDir)
for doc in listOfDocs:
    print doc
    f = open(rootDir + "/" + doc, "r")

    fileContents = f.read()

    data = re.split(regex, fileContents)

    datalist = data[0].split()

    datalistAfterStopping = []
    datalistAfterFormatting = []
    datalistAfterStemming = []

    for word in datalist:
        tempWord = string.lower(word)
        tempWord = string.strip(tempWord, "!@#$%^&*()_+=-<>?,./';}{[]\|~:")
        tempWord = string.replace(tempWord, "\"", "", -1)
        tempWord = string.replace(tempWord, "'", "", -1)
        if (not stoplist.__contains__(tempWord)) and len(tempWord) > 0:
            datalistAfterStopping.append(tempWord)

    map(lambda x: datalistAfterFormatting.extend(pattern.split(x)) ,datalistAfterStopping)
    i =0
    map(lambda x: datalistAfterStemming.append(p.stem(x, 0, len(x)-1)) if (len(x) > 0 and not invalidDir.__contains__(x)) else i,datalistAfterFormatting)

    datalistAfterStemmingLength = len(datalistAfterStemming)
    print datalistAfterStopping
    print datalistAfterFormatting
    print datalistAfterStemming
    doclen[doc] = len(datalistAfterStemming)

    map(lambda x: open("index/" + x,"a+b").write(doc.split(".")[0] + " " + str(datalistAfterStemmingLength) + " " + str(datalistAfterStemming.count(x)) + "\n"), Set(datalistAfterStemming))

if not os.path.exists("avgdoclen"):
    os.mkdir("avgdoclen")

open("avgdoclen/avgdoclen","w").write(str(float(sum(doclen.values())/len(doclen))) + " " + str(sum(doclen.values())))
# print p.stem("abduction", 0, 8)

# The index will typically consist of multiple files: 
# 1. for each term - create a file - 
# 
#     term1 
#     docID1 doclen1 termfrequency1
#     docID2 doclen2 termfrequency2
#     docID3 doclen3 termfrequency3
#     docID4 doclen4 termfrequency4
# -- combine all files created in previous step
# 
# 2. file containing list of unique terms :
#     for each term: 
#         term1 inverted_index_offset documentFrequency cummulativeTermFreq
# 1. map term name - term IDs