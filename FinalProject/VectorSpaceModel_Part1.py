from collections import OrderedDict
from ReadingIndexNoStopping import ReadIndex
from ParseQuery import ParseQuery
import os

r = ReadIndex()
pq = ParseQuery()

avg_doclen = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[0])

QueryMappedWithNumbers = pq.getParsedQuery()

QueryLength = {}

for key,value in QueryMappedWithNumbers.items():
    QueryLength[key] = len(value)
    
OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

avgQueryLength = float(sum(QueryLength.values()))/float(len(QueryLength))

print avgQueryLength

if not os.path.exists("OutputQueryNS_DB_S"):
    os.mkdir("OutputQueryNS_DB_S")

foutput = open("OutputQueryNS_DB_S/Output_Part1.txt", "w")

for (key,value) in OrderedQueriesMappedWithNumbers.items():
    
    DocumentstermWeightPerTerm = {}
    query_number = key
    current_query_length = float(len(value))
    print query_number
    print value
    for term in value:
        okapiTF_query_curr_term = float(value.count(term)) / (float(value.count(term)) + 0.5 + 1.5*current_query_length/avgQueryLength)        

        numbers = r.read(term)
        if len(numbers) > 0:
            inverted_list = map(lambda i: (numbers[2 + 3*i],
                                      float(numbers[3 + 3*i]),
                                   float(numbers[4 + 3*i]))
                            ,range(0, (len(numbers) - 2)/3))
        
            for (docid,doclen,tf) in inverted_list:
                okapiTF_document_curr_term = tf / (tf + 0.5 + 1.5*doclen/avg_doclen)
                if DocumentstermWeightPerTerm.has_key(docid):
                    DocumentstermWeightPerTerm[docid] += float(okapiTF_document_curr_term*okapiTF_query_curr_term)                
                else: 
                    DocumentstermWeightPerTerm[docid] = float(okapiTF_document_curr_term*okapiTF_query_curr_term)
                
    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))
    
    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + key + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1

foutput.close()