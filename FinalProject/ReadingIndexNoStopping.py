import linecache

class ReadIndex:
      
    def read(self,word):
        f = open("IndexFullNoStopping/firstIndex","r")
        
        output = []
        for line in f:
            words = line.split()
            if words[0] == word:
#                 print words[0] + " - " + words[2] + " - " + words[3]
                output.append(float(words[2]))
                output.append(float(words[3]))
                n = int(words[2])
                offset = int(words[1])
                while n >0:
                    output.extend(linecache.getline("IndexFullNoStopping/secondIndex", offset).strip().split())
                    offset += 1      
                    n -= 1
        return output