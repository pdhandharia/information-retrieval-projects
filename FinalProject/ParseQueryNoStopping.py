import re
from sets import Set
import string
from PorterStem import PorterStemmer

class ParseQuery:
    
    def getParsedQuery(self):
        p = PorterStemmer()
        pattern = re.compile(r'\W+')
        
        f = open("cacm.query")
        f_stoplist = open("stoplist.txt","r")
        stoplist = Set()
        
        for line in f_stoplist:
            line = string.strip(line, "\n")
            stoplist.add(line)
        
        text = f.read().split()
        
        output = []
        temp = []
        formattedQueryMap = {}
        
        for element in text:
            if element == '</DOC>':
                output.append(temp)
                temp = []
            else: 
                temp.append(element)
        
        for query in output:
            datalistAfterStopping = []
            datalistAfterFormatting = []
            datalistAfterStemming = []
            for term in query[4:len(query)]:
                tempWord = string.strip(string.lower(term), "!@#$%^&*()_+=-<>?,./';}{[]\|~:")
                tempWord = string.replace(tempWord, "\"", "", -1)
                tempWord = string.replace(tempWord, "'", "", -1)
                if len(tempWord) > 0:
                    datalistAfterStopping.append(tempWord)
                    
            map(lambda x: datalistAfterFormatting.extend(pattern.split(x)) ,datalistAfterStopping)
            i =0
            map(lambda x: datalistAfterStemming.append(p.stem(x, 0, len(x)-1)) if len(x) > 0 else i,datalistAfterFormatting)
            formattedQueryMap[int(query[2])] = datalistAfterStemming
        return formattedQueryMap
#         for key,value in formattedQueryMap.items():
#             print key
#             print value