import string
from sets import Set
import urllib, re
from collections import OrderedDict
import math
from ReadingIndexNoStopping import ReadIndex
from ParseQuery import ParseQuery
import os

r = ReadIndex()
pq = ParseQuery()
rootDir = "cacm"

QueryMappedWithNumbers = {}

avg_doclen = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[0])
totalNumDocs = len(os.listdir(rootDir))
numOfUniqueWords = len(os.listdir("indexNoStopping"))
lambda_lm = 0.8
numOfterms = float(open("avgdoclenNoStopping/avgdoclen","r").read().split()[1])

QueryMappedWithNumbers = pq.getParsedQuery()
    
OrderedQueriesMappedWithNumbers = OrderedDict(sorted(QueryMappedWithNumbers.items(), key=lambda t: t[0],reverse=False))

if not os.path.exists("OutputQueryNS_DB_S"):
    os.mkdir("OutputQueryNS_DB_S")

foutput = open("OutputQueryNS_DB_S/Output_Part4.txt", "w")   
 
for (key,value) in OrderedQueriesMappedWithNumbers.items():
     
    DocumentstermWeightPerTerm = {}
    SeenWords = {}
    DocLengthHash = {}
    ctf_term = {}
    query_number = key
    current_query_length = len(value)
    print query_number
    print value
    for term in value:
        numbers = r.read(term)
        if len(numbers) > 0:
            inverted_list = map(lambda i: (numbers[2 + 3*i],
                                      float(numbers[3 + 3*i]),
                                   float(numbers[4 + 3*i]))
                            ,range(0, (len(numbers) - 2)/3))
            
            df,ctf = float(numbers[0]), float(numbers[1])
            ctf_term[term] = ctf
            lm_jk_q = ctf / numOfterms
            
            for (docid,doclen,tf) in inverted_list:
                lm_jk_p = tf/doclen
                
                p_lm_jk_term = math.log((lambda_lm* lm_jk_p) + ((1-lambda_lm)*lm_jk_q))
                
                #/(current_query_length*doclen)
                if DocumentstermWeightPerTerm.has_key(docid):
                    DocumentstermWeightPerTerm[docid] += p_lm_jk_term
                    SeenWords[docid].append(term)
                else: 
                    DocumentstermWeightPerTerm[docid] = p_lm_jk_term
                    SeenWords[docid] = [term]
                                    
    for doc in DocumentstermWeightPerTerm.keys():
        for t in value:
            if t not in SeenWords[doc] and ctf_term.has_key(t):
                DocumentstermWeightPerTerm[doc] += math.log((1-lambda_lm)*(ctf_term[t]/numOfterms))
    
    DocumentstermWeightPerTermOrdered = OrderedDict(sorted(DocumentstermWeightPerTerm.items(), key=lambda t: t[1],reverse=True))

    rank = 1
    for key,value in DocumentstermWeightPerTermOrdered.items():
        foutput.write(str(query_number) + " Q0 " + key + " " + str(rank) + " " + str(value) + " Exp\n")
        if rank == 1000:
            break
        rank +=1
 
foutput.close()