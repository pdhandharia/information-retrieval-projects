import os
import time,re

if not os.path.exists("IndexFullDeltaEncoding"):
    os.mkdir("IndexFullDeltaEncoding")
time.sleep(1)

firstIndex = open("IndexFullDeltaEncoding/firstIndex","w")
secondIndex = open("IndexFullDeltaEncoding/secondIndex","w")
documentMap = open("IndexFullDeltaEncoding/documentMap", "w")

listOfindexes = os.listdir("index")
pattern = re.compile(r'\W+')
docMap = {}

print len(listOfindexes)

offset = 1

for index in listOfindexes:
    with open("index/" + index,"r") as f:
        numOfLines = 0
        ctf = 0
        sumDocid = 0
        for line in f:
            numOfLines += 1
            ctf += int(line.split()[2])
            (docid,doclen,tf) = line.split()
            internalDocid = int(pattern.split(docid)[1])
            curr = internalDocid - sumDocid
            sumDocid += curr
            secondIndex.write(str(curr) + " " + str(tf) + "\n")
            if not docMap.has_key(internalDocid):
                docMap[internalDocid] = str(docid) + " " +  str(doclen)
#             secondIndex.write(line)
    print index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf)
    firstIndex.write(index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf) + "\n")
    offset += numOfLines

for key,value in docMap.items():
    documentMap.write(str(key) + " " + value + "\n")

firstIndex.close()
secondIndex.close()
documentMap.close()
