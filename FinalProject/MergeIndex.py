import os
import time

if not os.path.exists("IndexFull"):
    os.mkdir("IndexFull")
time.sleep(1)

firstIndex = open("IndexFull/firstIndex","w")
secondIndex = open("IndexFull/secondIndex","w")

listOfindexes = os.listdir("index")

print len(listOfindexes)

offset = 1

for index in listOfindexes:
    with open("index/" + index,"r") as f:
        numOfLines = 0
        ctf = 0
        for line in f:
            numOfLines += 1
            secondIndex.write(line)
            ctf += int(line.split()[2])
    print index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf)
    firstIndex.write(index + " " + str(offset) + " " + str(numOfLines) + " " + str(ctf) + "\n")
    offset += numOfLines

firstIndex.close()
secondIndex.close()
